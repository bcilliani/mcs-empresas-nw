package com.empresa.trabajo.gradle.service;

import java.util.List;

import com.empresa.trabajo.gradle.model.Empresa;
import com.empresa.trabajo.gradle.model.Recibo;

public interface EmpresaService {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);
	void saveRecibo(Recibo recibo);
	
}

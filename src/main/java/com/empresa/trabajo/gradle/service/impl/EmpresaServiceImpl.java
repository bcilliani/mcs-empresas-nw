package com.empresa.trabajo.gradle.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empresa.trabajo.gradle.dao.impl.EmpresaDaoImpl;
import com.empresa.trabajo.gradle.model.Empresa;
import com.empresa.trabajo.gradle.model.Recibo;
import com.empresa.trabajo.gradle.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmpresaDaoImpl _empresalDao;
	
	@Override
	public List<Empresa> getAllEmpresas() {
		
		return _empresalDao.getAllEmpresas();
	}

	@Override
	public Empresa getEmpresa(Integer id) {
		
		return _empresalDao.getEmpresa(id);
	}

	@Override
	public void saveEmpresa(Empresa empresa) {
		try {
			_empresalDao.saveEmpresa(empresa);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		try {
			_empresalDao.saveRecibo(recibo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}

}

package com.empresa.trabajo.gradle.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.empresa.trabajo.gradle.dao.EmpresaDao;
import com.empresa.trabajo.gradle.model.Empresa;
import com.empresa.trabajo.gradle.model.Recibo;
import com.empresa.trabajo.gradle.rowmapper.EmpresaRowMapper;

@Repository
public class EmpresaDaoImpl extends JdbcDaoSupport implements EmpresaDao {

	public EmpresaDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Empresa> getAllEmpresas() {
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id_empresa, ruc, razon_social, estado_actual\n" + 
				" FROM microservicios.empresa";
		
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			logger.debug("Se han listado "+listaEmpresas.size()+" empresas");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaEmpresas;
	}

	@Override
	public Empresa getEmpresa(Integer id) {
		logger.debug("::::: Mensaje de prueba :::::::");
		Empresa empresa = new Empresa();	
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id_empresa, ruc, razon_social, estado_actual\n" + 
				" FROM microservicios.empresa where id='"+id+"'";
				
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			
			empresa = listaEmpresas.get(0);
			
			logger.debug("Se ha traido a la empresa "+listaEmpresas.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return empresa;
	}

	@Override
	public void saveEmpresa(Empresa empresa) {
		
		String sql = "insert into microservicios.empresa (ruc, razon_social, estado_actual) "  
				+ "values (?, ?, ?);";
		
		Object[] params = { empresa.getRuc(), empresa.getRazonSocial(), empresa.getEstado()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la persona "+empresa.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	@Override
	public void saveRecibo(Recibo recibo) {
		
		String sql = "insert into microservicios.recibo (id_empresa, rno_recibo, monto_emitido, fg_retencion) "  
				+ "values (?, ?, ?);";
		
		Object[] params = { recibo.getId(), recibo.getNumero(), recibo.getMonto(),recibo.getFlgRetencion()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado al recibo "+recibo.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

}

package com.empresa.trabajo.gradle.dao;

import java.util.List;

import com.empresa.trabajo.gradle.model.Empresa;
import com.empresa.trabajo.gradle.model.Recibo;

public interface EmpresaDao {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);
	void saveRecibo(Recibo Recibo);

}

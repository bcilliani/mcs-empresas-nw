package com.empresa.trabajo.gradle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.trabajo.gradle.model.Empresa;
import com.empresa.trabajo.gradle.model.Recibo;
import com.empresa.trabajo.gradle.service.impl.EmpresaServiceImpl;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@GetMapping(value = "/", produces = "application/json")	
	public List<Empresa> getAllEmpresas(){
		return _empresaService.getAllEmpresas();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")	
	public Empresa getEmpresa(@PathVariable ("id") Integer id){
		return _empresaService.getEmpresa(id);
	}
	
	@PostMapping(value = "/", produces = "application/json")	
	public List<Empresa> saveEmpresa(@RequestBody Empresa empresa){
		
		_empresaService.saveEmpresa(empresa);
		
		return _empresaService.getAllEmpresas();
	}	
	
	@PostMapping(value = "/recibo", produces = "application/json")	
	public Empresa saveRecibo(@RequestBody Recibo recibo){
		
		_empresaService.saveRecibo(recibo);
		
		return _empresaService.getEmpresa(recibo.getId());
	}	

}

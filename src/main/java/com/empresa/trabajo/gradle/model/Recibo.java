package com.empresa.trabajo.gradle.model;

public class Recibo {
	
	private Integer id;
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getFlgRetencion() {
		return flgRetencion;
	}

	public void setFlgRetencion(String flgRetencion) {
		this.flgRetencion = flgRetencion;
	}


	private String numero;
	private double monto;
	private String flgRetencion;
	
		
	public Recibo() {
		
	}
	
	public Recibo(Integer id, String numero, double monto, String flgRetencion) {
	
		this.id = id;
		this.numero = numero;
		this.monto = monto;
		this.flgRetencion = flgRetencion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	@Override
	public String toString() {
		return "Persona [id=" + id + ", numeroRecibo=" + numero + ", monto=" + monto
				+ ", flgRetencion=" + flgRetencion + "]";
	}
	
}

package com.empresa.trabajo.gradle.model;

public class Empresa {
	
	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	private Integer id;
	private String ruc;
	private String razonSocial;
	private String estado;
	
		
//	
	
	public Empresa() {
		
	}
	
	public Empresa(Integer id, String ruc, String razonSocial, String estado) {
	
		this.id = id;
		this.ruc = ruc;
		this.razonSocial = razonSocial;
		this.estado = estado;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", ruc=" + ruc + ", razonSocial=" + razonSocial
				+ ", estado=" + estado  + "]";
	}
	
}

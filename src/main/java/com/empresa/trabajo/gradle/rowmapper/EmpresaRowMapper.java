package com.empresa.trabajo.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.empresa.trabajo.gradle.model.Empresa;

public class EmpresaRowMapper implements RowMapper<Empresa>{

	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		Empresa empresa = new Empresa();
		
		empresa.setId(rs.getInt("id"));
		empresa.setRuc(rs.getString("ruc"));
		empresa.setRazonSocial(rs.getString("razon_social"));
		empresa.setEstado(rs.getString("estado_actual"));
		
		return empresa;
	}

}
